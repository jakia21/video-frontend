import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullComponentComponent } from './Layouts/full-component/full-component.component';
import { DashboardComponent } from './Layouts/dashboard/dashboard.component';

import { HomeComponent } from './Components/home/home.component';
import { CampaignDetailsComponent } from './Components/campaign-details/campaign-details.component';
import { ChangePlatformComponent } from './Components/change-platform/change-platform.component';
import { ChangeFormatComponent } from './Components/change-format/change-format.component';
import { ChangeTemplateComponent } from './Components/change-template/change-template.component';
import { EditTemplateComponent } from './Components/edit-template/edit-template.component';
import { EditMusicComponent } from './Components/edit-music/edit-music.component';
import { ExportComponent } from './Components/export/export.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: '',
    component: FullComponentComponent,
    children: [
      {
        path: 'dashboard',
        component: HomeComponent
      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
     {
        path: 'campaign-details',
        component: CampaignDetailsComponent
      },
      {
        path: 'change-platform',
        component: ChangePlatformComponent
      },
      {
        path: 'change-format',
        component: ChangeFormatComponent
      },
      {
        path: 'change-template',
        component: ChangeTemplateComponent
      },
      {
        path: 'edit-template',
        component: EditTemplateComponent
      },
      {
        path: 'edit-music',
        component: EditMusicComponent
      },
      {
        path: 'export',
        component: ExportComponent
      }

    ]
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class VideoAppRoutes { }
