import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private toastr: ToastrService
  ) { }

  success(message: string, title?: string, overide?) {
    return this.toastr.success(message, title, overide);
  }

  error(message: string, title?: string, overide?) {
    return this.toastr.error(message, title, overide);
  }
}
