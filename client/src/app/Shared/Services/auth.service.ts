import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserAuth } from '../Models/UserAuth';
import { User } from '../Models/User';
import { ResetPassword } from '../Models/ResetPassword';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

import { ChangePassword } from '../Models/ChangePassword';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.apiUrl;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
  }

  public get currentUser(): User {
    return this.currentUserSubject.value;
  }

  getToken(auth: UserAuth): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/auth/token`, auth)
      .pipe(map(authResponse => {
        // login successful if there's a jwt token in the response
        if (authResponse && authResponse.response.token) {
          const loginObj = {
            ...authResponse.response.user,
            token: authResponse.response.token
          };
          // store user details and jwt token in local storage to keep user
          // logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(loginObj));
          this.currentUserSubject.next(loginObj);
        }
        return authResponse;
      }));
  }

  logout(serverLogout = true) {
    if (serverLogout) {
      this.revokeToken({});
    }
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['/login']);
  }

  revokeToken(auth: UserAuth): Observable<UserAuth> {
    return this.http.post<any>(`${this.apiUrl}/auth/revokeToken`, auth);
  }

  revokeOtherTokenAndGetNew(auth: UserAuth): Observable<UserAuth> {
    return this.http.post<any>(`${this.apiUrl}/auth/revokeExistingToken`, auth);
  }

  validateToken() {
    return this.http.post<any>(`${this.apiUrl}/auth/validateToken`, null)
      .pipe(
        catchError(() => {
          this.logout(false);
          return null;
        })
      );
  }

  requestPasswordReset(email: UserAuth): Observable<UserAuth> {
    return this.http.post<UserAuth>(`${this.apiUrl}/auth/requestPasswordReset`, email);
  }

  changePassword(changePassword: ChangePassword): Observable<UserAuth> {
    return this.http.post<ChangePassword>(`${this.apiUrl}/auth/changePassword`, changePassword);

  }

  resetPassword(resetPassword: ResetPassword): Observable<ResetPassword> {
    return this.http.post<ResetPassword>(`${this.apiUrl}/auth/resetPassword`, resetPassword);
  }
  getAuthToken() {
    const user = this.currentUserSubject.value;
    return user.token;
  }
}
