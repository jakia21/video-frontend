import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { Config } from './config';

declare var $:any;

@Injectable({
  providedIn: 'root'
})
export class CommmonService {
  config = new Config;
  constructor(private http: HttpClient, private route: Router) { }
  public refreshPage: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  getAppInit(userId?: number): Observable<any>{
    return this.http.post(`${this.config.url}/common/list`, {userId})
  };
  setStorage(name, value) {
    // Get the existing data
    var existing = localStorage.getItem('userDetails');

    // If no existing data, create an array
    // Otherwise, convert the localStorage string to an array
    existing = existing ? JSON.parse(existing) : {};

    // Add new data to localStorage Array
    existing[`${name}`] = value;


    // Save back to localStorage
    localStorage.setItem('userDetails', JSON.stringify(existing));
  }


  deleteTempalate(){
    let delKey = JSON.parse(localStorage.getItem('userDetails'));
    let deletedItem = ['listTemplates']
    for (let i of deletedItem) {
      delete delKey[i]
    }
    localStorage.setItem('userDetails', JSON.stringify(delKey))
  }
  searchTemplateById(res, template, platform) {
    let obj = {
      'selectedTemplate': {},
      'selectedTheme': []
    }
    const { image_url, name, short_detail, category_id, formats, plateforms, id } = res[0];

    const getPlatformImg = platform.filter((item) => {
      return item.id == plateforms
    })

    Object.assign(obj['selectedTemplate'], { name, image_url, short_detail, 'platform_img': getPlatformImg[0].image_url, category_id, formats, plateforms, id })
    Object.assign(obj['selectedTheme'], res[0].themes);
    this.setStorage('userJourney', obj);
    setTimeout(() => { this.updateDevice(res[0].themes[0].filename_image_url, name)},100);
    window['_platform'].refreshPage();
  }

  updateDevice(imgurl, temp_name){
    // Get the existing data
    $('.deviceHtml .templateName').text(temp_name)
    $('.deviceHtml').css('background-image','url('+imgurl+')')
    // Save back to localStorage
    const html = JSON.stringify($('.deviceHtml').get(0).outerHTML);
    localStorage.setItem('device', html);
  }




}
