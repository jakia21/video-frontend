import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Config } from './config';
import { saveData } from '../Models/userChanges'

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  config = new Config;
  constructor(private http: HttpClient) { }

  getTemplateList(): Observable<any> {
    return this.http.post(`${this.config.url}/template/list`, null)
  }
  editTemplaterouteToNext(): Observable<any> {
    return this.http.post(`${this.config.url}/template/list`, null)
  }
  getThemeListById(categoryId: number, templateId: number): Observable<any> {
    return this.http.post(`${this.config.url}/theme/list`, {categoryId, templateId  })
  }
  getSlideList(themeId: number): Observable<any>{
    return this.http.post(`${this.config.url}/slide/list`, {themeId} )
  }
  setEditor(userId: number, campaignId: number, themeId: number){
    return this.http.post(`${this.config.url}/editor/set`, {userId, campaignId, themeId} )
  }
  getEditor(userId: number, campaignId: number, themeId: number, editorId: number) {
    return this.http.post(`${this.config.url}/editor/get`, { userId, campaignId, themeId, editorId })
  }

  saveUserChanges(
    userId: number,
    campaignId: number,
    themeId: number,
    slideId: number,
    elementId: number,
    editorId: number,
    type: string,
    value: string): Observable<any> {
    return this.http.post(`${this.config.url}/editable/set`, {
      userId,
      campaignId,
      themeId,
      slideId,
      elementId,
      editorId,
      type,
      value
     })
  }

  // find index in array
  findIndexInData(optionsArg) {
    var options = {
      data: optionsArg.data,
      where: optionsArg.where,
      what: optionsArg.what
    },
      result = -1;
    options.data.some(function (item, i) {
      if (item[options.where] === options.what) {
        result = i;
        return true;
      }
    });
    return result;
  }

 shadeColor(color, percent) {
  let R = parseInt(color.substring(1, 3), 16);
  let G = parseInt(color.substring(3, 5), 16);
  let B = parseInt(color.substring(5, 7), 16);

   R = Number(R * Number(100 + percent) / 100);
   G = Number(G * Number(100 + percent) / 100);
   B = Number(B * Number(100 + percent) / 100);

  R = (R < 255) ? R : 255;
  G = (G < 255) ? G : 255;
  B = (B < 255) ? B : 255;

  let RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
  let GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
  let BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

  return "#" + RR + GG + BB;
}

}
