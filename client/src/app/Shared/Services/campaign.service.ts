import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, from } from 'rxjs';
import { environment } from '../../../environments/environment';

import { createCampign } from '../Models/createCampaign'
import { Config } from './config';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  config = new Config;
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient, private route: Router) { }

  getCategoryList(): Observable<any>{
    return this.http.post(`${this.config.url}/category/list`, null)
  }
  getPlatform(): Observable<any> {
    return this.http.post(`${this.config.url}/plateform/list`, null)
  }
  getFormat(): Observable<any> {
    return this.http.post(`${this.config.url}/format/list`, null)
  }

  createCampaign(create: createCampign ): Observable<any> {
    return this.http.post(`${this.config.url}/campaign/create`, create)
  }
  getAllCampaignList(): Observable<any> {
    return this.http.post(`${this.config.url}/campaign/list`, null)
  }

  fetchTemplate(): Observable<any> {
    return this.http.post(`${this.config.url}/template/list`, null)
  }

  setStorageforUser(name, value){
    // Get the existing data
    var existing = localStorage.getItem('userDetails')['userJourney'];

    // If no existing data, create an array
    // Otherwise, convert the localStorage string to an array
    existing = existing ? JSON.parse(existing) : {};

    // Add new data to localStorage Array
    existing[`${name}`] = value;


    // Save back to localStorage
    localStorage.setItem('userDetails', JSON.stringify(existing));
  }

  setStorage(name, value) {
    // Get the existing data
    var existing = localStorage.getItem('userDetails');

    // If no existing data, create an array
    // Otherwise, convert the localStorage string to an array
    existing = existing ? JSON.parse(existing) : {};

    // Add new data to localStorage Array
    existing[`${name}`] = value;


    // Save back to localStorage
    localStorage.setItem('userDetails', JSON.stringify(existing));
  }

  routeToNext(url){
      this.route.navigate([url]);
  }

  removeEditorInfo(){
    let data = JSON.parse(localStorage.getItem('userDetails'))
    for (let i in data) {
      if (String([i]) == 'editorDetails') {
        delete data[i]
      }
    }
    localStorage.setItem("userDetails", JSON.stringify(data));

  }


}
