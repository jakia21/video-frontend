import { HttpHeaders } from '@angular/common/http';

export class Config {
  url: string = 'http://35.154.173.85:3333/api/v1';
  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }
}
