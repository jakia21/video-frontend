export class Project {
  name?: string;
  uuid?: string;
  assetType?: string;
  status?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
