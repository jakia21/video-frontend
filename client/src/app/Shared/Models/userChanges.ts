export class saveData{
  userId: number;
  campaignId: number;
  themeId: number;
  slideId: number;
  elementId: number;
  editorId: number;
  type: string;
  value: string;
}
