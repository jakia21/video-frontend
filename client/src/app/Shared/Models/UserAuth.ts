export class UserAuth {
  email?: any;
  password?: string;
  token?: string;
  newPassword?: string;
  resetToken?: string;
  agree?: boolean;
}
