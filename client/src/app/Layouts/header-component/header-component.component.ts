import { Component, OnInit } from '@angular/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css'],
  providers: [NgbDropdownModule]
})
export class HeaderComponentComponent implements OnInit {
  userInfo = {
    email: '', fname: '', lname: ''
  };
  constructor() { }

  ngOnInit() {
    const { email, fname, lname } = JSON.parse(localStorage.getItem('userDetails')).currentUser;
    Object.assign(this.userInfo, { email, fname, lname})
  }

}
