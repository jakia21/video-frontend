import { Component, OnInit } from '@angular/core';
import { CommmonService } from '../../Shared/Services/common.service';

@Component({
  selector: 'app-full-component',
  templateUrl: './full-component.component.html',
  styleUrls: ['./full-component.component.css']
})
export class FullComponentComponent implements OnInit {
  isLoading = false;
  data : any;
  constructor(public _http: CommmonService)
   { }

  ngOnInit() {
    this.fetchInital();
  }

  fetchInital() {
    localStorage.clear();
    this._http.getAppInit(1).subscribe(res => {
        if(res){
          const { currentUser, currentUserCampaigns, listFormats, listPlateforms, listSubformats, listTemplates, currentUserPreference } = res.statusData.data;
          localStorage.setItem('userDetails', JSON.stringify({ currentUser, currentUserCampaigns, listSubformats, currentUserPreference, listFormats, listPlateforms, listTemplates}))
          this.isLoading = true;
        }
    },(error)=>{
      console.error(error)
    })
  }



}
