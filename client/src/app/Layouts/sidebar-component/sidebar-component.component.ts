import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../Shared/Services/message.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-sidebar-component',
  templateUrl: './sidebar-component.component.html',
  styleUrls: ['./sidebar-component.component.css']
})
export class SidebarComponentComponent implements OnInit {
  campignName = '';
  isActiveCampign = false;
  constructor(private route: Router, private _msg: MessageService) {
    window['_sidebar'] = this;
  }

  ngOnInit() {
    if(localStorage.getItem('userDetails') === null){
      this.route.navigate(['/']);
      return false;

    }
    setTimeout(() => this.checkIfCampignExists());

  }

  checkIfCampignExists(){
    const isActive = JSON.parse(localStorage.getItem('userDetails'));

    if (isActive.campignDetails === undefined || isActive.campignDetails === null){
      this._msg.error('Please enter campaign details',);
      this.route.navigate(['/dashboard/campaign-details']);
      return false;
    }
    if (isActive.campignDetails !== undefined || isActive.campignDetails !== null) {
      if (isActive.campignDetails[0] !== undefined){
        this.campignName = isActive.campignDetails[0].name;
        this.isActiveCampign = true;
      }
    }




  }

}
