import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../app/Shared/Interceptors/jwt.interceptor';
import { ErrorInterceptor } from '../app/Shared/Interceptors/error.interceptor';
import { GlobalConstants } from '../app/Shared/constants/globalConstant';
import { ColorPickerModule } from 'ngx-color-picker';
import { Ng5SliderModule } from 'ng5-slider';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ImageCropperModule } from 'ngx-image-cropper';


import { BlankComponentComponent } from './Layouts/blank-component/blank-component.component';
import { FullComponentComponent } from './Layouts/full-component/full-component.component';
import { SidebarComponentComponent } from './Layouts/sidebar-component/sidebar-component.component';
import { HeaderComponentComponent } from './Layouts/header-component/header-component.component';
import { LogoutComponentComponent } from './Layouts/logout-component/logout-component.component';
import { LoginComponent } from './Components/authentication/login/login.component';
import { LogoutComponent } from './Components/authentication/logout/logout.component';
import { CampaignDetailsComponent } from './Components/campaign-details/campaign-details.component';
import { ChangePlatformComponent } from './Components/change-platform/change-platform.component';
import { ChangeFormatComponent } from './Components/change-format/change-format.component';
import { ChangeTemplateComponent } from './Components/change-template/change-template.component';
import { EditTemplateComponent } from './Components/edit-template/edit-template.component';
import { EditMusicComponent } from './Components/edit-music/edit-music.component';
import { ExportComponent } from './Components/export/export.component';
import { PreviewDeviceComponent } from './Components/core/preview-device/preview-device.component';
import { FooterComponent } from './Layouts/footer/footer.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoAppRoutes } from './app-routing.module';
import { HomeComponent } from './Components/home/home.component';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { DashboardComponent } from './Layouts/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { PlatformComponent } from './Components/core/platform/platform.component';
import { PlatformSidebarComponent } from './Components/core/platform-sidebar/platform-sidebar.component';
import { FormatComponent } from './Components/core/format/format.component';
import { LoaderComponent } from './Components/core/loader/loader.component';
import { SkeletonLoadingComponent } from './Components/core/skeleton-loading/skeleton-loading.component';
import { UserCampaignComponent } from './Components/core/user-campaign/user-campaign.component';
import { TemplateWidgetComponent } from './Components/core/template-widget/template-widget.component';
import { EditTemplateWidgetComponent } from './Components/core/edit-template-widget/edit-template-widget.component';

@NgModule({
  declarations: [
    BlankComponentComponent,
    FullComponentComponent,
    SidebarComponentComponent,
    HeaderComponentComponent,
    LogoutComponentComponent,
    LoginComponent,
    LogoutComponent,
    CampaignDetailsComponent,
    ChangePlatformComponent,
    ChangeFormatComponent,
    ChangeTemplateComponent,
    EditTemplateComponent,
    EditMusicComponent,
    ExportComponent,
    PreviewDeviceComponent,
    FooterComponent,
    HomeComponent,
    AppComponent,
    DashboardComponent,
    PlatformComponent,
    PlatformSidebarComponent,
    FormatComponent,
    LoaderComponent,
    SkeletonLoadingComponent,
    UserCampaignComponent,
    TemplateWidgetComponent,
    EditTemplateWidgetComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    VideoAppRoutes,
    ReactiveFormsModule,
    ColorPickerModule,
    Ng5SliderModule,
    SlickCarouselModule,
    ImageCropperModule
  ],
  providers: [
    [
      GlobalConstants,
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    //private _const: ConstantService // To load constants at the time of instantiating of application
  ) { }
}
