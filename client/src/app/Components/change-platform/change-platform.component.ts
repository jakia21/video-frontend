import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CampaignService } from '../../Shared/Services/campaign.service';
import { GlobalConstants } from '../../Shared/constants/globalConstant'
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-change-platform',
  templateUrl: './change-platform.component.html',
  styleUrls: ['./change-platform.component.css']
})
export class ChangePlatformComponent implements OnInit {
  details = {
    brand: '', name: '', product: '', platform_img: '', properties_duration: '', properties_aspect_ratio: '', subformat_id: 1
  };
  platformIcon:any;
  isloaded = false;
  constructor(private _http: CampaignService, private constant: GlobalConstants, private router: Router) {

  }

  ngOnInit() {
    this.setPreviewScreen();
  }
  ngAfterViewInit(){
  }

  setPreviewScreen(){
    const html = JSON.parse(localStorage.getItem('device'));
    $('.mockup-content').html(html);

  }
  fw(){
    this._http.routeToNext('/dashboard/change-format')
  }

}
