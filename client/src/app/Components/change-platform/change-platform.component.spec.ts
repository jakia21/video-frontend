import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePlatformComponent } from './change-platform.component';

describe('ChangePlatformComponent', () => {
  let component: ChangePlatformComponent;
  let fixture: ComponentFixture<ChangePlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
