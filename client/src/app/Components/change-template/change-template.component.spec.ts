import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeTemplateComponent } from './change-template.component';

describe('ChangeTemplateComponent', () => {
  let component: ChangeTemplateComponent;
  let fixture: ComponentFixture<ChangeTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
