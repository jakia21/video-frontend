import { Component, OnInit } from '@angular/core';
import { CampaignService } from '../../Shared/Services/campaign.service';
import { CommmonService } from '../../Shared/Services/common.service';
declare var $:any;

@Component({
  selector: 'app-change-template',
  templateUrl: './change-template.component.html',
  styleUrls: ['./change-template.component.css']
})
export class ChangeTemplateComponent implements OnInit {
  activeTab = 'template';
  data: any;
  filterData: any;
  category: any;
  selectedCategory = 'All';
  constructor(private _http: CampaignService, public _temp: CommmonService) { }

  ngOnInit() {
    this.fetchTemplate();
    this.fetchCategory();
  }
  fetchTemplate(){
    this._http.fetchTemplate().subscribe(response =>{
      this.data = response.statusData.data;
      this.filterData = this.data;
    })
  }
  fetchCategory(){
    this._http.getCategoryList().subscribe(res => {
      if(res){
        this.category = res.statusData.data
      }
    })
  }
  filterById(id, name){
    this.selectedCategory = name;
    const filter = this.data.filter((item)=>{
          return item.category_id === id
    });
    this.filterData = filter;
  }
  showAll(name){
    this.selectedCategory = name;
    this.filterData = this.data;
  }
  fetchTemplateById(id){
    const filter = this.data.filter((item) => {
      return item.id === id
    });
    const platform = JSON.parse(localStorage.getItem('userDetails'))
    this._temp.searchTemplateById(filter, '', platform.listPlateforms)
    this._http.removeEditorInfo();
  }



}
