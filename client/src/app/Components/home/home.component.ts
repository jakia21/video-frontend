import { Component, OnInit } from '@angular/core';
import { CommmonService } from '../../Shared/Services/common.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isSelected = false;
  isFetched = false;
  template: any;
  platform : any;
  selected = {
    name: '',
    image_url: '',
    short_detail: '',
    template_id : '',
    category_id: '',
    platform_img: '',
    formats: '',
    subformats: '',
    selectedTheme: ''
  }
  theme = [];
  constructor(
    private _http: CommmonService,
    private router: Router
  ) { }

  ngOnInit() {

  }
  ngAfterViewInit(){
    setTimeout(()=> { this.fetchData()},500);
  }

  fetchData() {
    const { listTemplates, listPlateforms } = JSON.parse(localStorage.getItem('userDetails'));
    this.template = listTemplates;
    this.platform = listPlateforms;
    this.isFetched = true;
    this._http.deleteTempalate();
  }

  getTemplate(templateid){
    this.searchTemplateById(templateid);
    this.isSelected = true;
  }
  searchTemplateById(id){
    const result =  this.template.filter((item)=>{
      return item.id == id;
    });
    const { image_url, name, short_detail, category_id, formats, plateforms } = result[0];

    const getPlatformImg = this.platform.filter((item)=>{
        return item.id == plateforms
    })

    Object.assign(this.selected, { name, image_url, short_detail, 'platform_img': getPlatformImg[0].image_url, category_id, formats, plateforms, id })
    Object.assign(this.theme, result[0].themes);

  }
  saveTemplate(){
    const html = JSON.stringify($('.deviceHtml').get(0).outerHTML);
    localStorage.setItem('device', html);

    this._http.setStorage('userJourney', {'selectedTemplate': this.selected, 'selectedTheme': this.theme})

    this.router.navigate(['/dashboard/campaign-details']);
  }


}
