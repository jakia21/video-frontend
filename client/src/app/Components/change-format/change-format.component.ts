import { Component, OnInit } from '@angular/core';
import { CampaignService } from '../../Shared/Services/campaign.service';
import { GlobalConstants } from '../../Shared/constants/globalConstant'
declare var $:any;

@Component({
  selector: 'app-change-format',
  templateUrl: './change-format.component.html',
  styleUrls: ['./change-format.component.css']
})
export class ChangeFormatComponent implements OnInit {
  details = {
    brand: '',
    name: '',
    product: '',
    platform_img: '',
    stime:'',
    stype: '',
    aspect: '',
    story: ''
  };

  platformIcon = '';
  constructor(public _http: CampaignService, private constant: GlobalConstants) { }

  ngOnInit() {
    this.setPreviewScreen();
  }

  setPreviewScreen() {
    const html = JSON.parse(localStorage.getItem('device'));
    $('.mockup-content').html(html);
  }

}
