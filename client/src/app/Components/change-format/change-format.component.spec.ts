import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeFormatComponent } from './change-format.component';

describe('ChangeFormatComponent', () => {
  let component: ChangeFormatComponent;
  let fixture: ComponentFixture<ChangeFormatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeFormatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeFormatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
