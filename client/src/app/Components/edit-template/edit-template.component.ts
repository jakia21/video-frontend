import { Component, OnInit, ViewChild} from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { TemplateService } from '../../Shared/Services/template.service';
import { Options } from 'ng5-slider';
import { MessageService } from 'src/app/Shared/Services/message.service';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../Shared/Services/config';
import { CampaignService } from 'src/app/Shared/Services/campaign.service';


declare var $:any;

@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.css']
})
export class EditTemplateComponent implements OnInit {
  config = new Config;
  bgColor = '#000000';
  logoColor= '#000000';
  showloader= false;
  receivedTheme : any;
  slideList : any;
  isloaded = false;
  value: number = 5;
  options: Options = {
    showTicks: true,
    stepsArray: [],
    translate: (value: number): string => {
      return value + ' sec';
    }
  };
  initDevice = {
    backgroundImage : ''
  }
  slideConfig = {
    "slidesToShow": 5,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": false,
    "infinite": false
  };
  globalConfig: any;
  userSavedHistory: any;


  constructor(private _http: TemplateService,
              private _service: CampaignService,
              private _req: HttpClient,
              private modalService: NgbModal,
              public _msg: MessageService) { }
  @ViewChild('content') modelImg;

  ngOnInit() {
   this.fetchTheme();
  }
  ngAfterViewInit(){
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    }, (reason) => {
    });
  }


  fetchTheme(){
    const { currentUser, campignDetails, userJourney: { selectedTheme }, editorDetails  } = JSON.parse(localStorage.getItem('userDetails'));
    // console.log(category_id, templateId)
    this._http.getSlideList(selectedTheme[0].id).subscribe(res=>{
        this.slideList = res.statusData.data;
        this.getSlideStartTime(this.slideList)
        if (editorDetails === undefined){
          this.setInit();
          this.isloaded = true;
        }
    });

    if(editorDetails !== undefined){
      const data =  JSON.parse(localStorage.getItem('userDetails'));
      this.globalConfig = data.editorDetails[0];
      this._http.getEditor(currentUser.id, campignDetails[0].id, selectedTheme[0].id, editorDetails[0].id).subscribe(res => {
        this.userSavedHistory = res['data'].editables;
        this.setUserEditedState();
        this.setInit();
        this.isloaded = true;
      })
    }else{
      this._http.setEditor(currentUser.id, campignDetails[0].id, selectedTheme[0].id).subscribe(res => {
        this.globalConfig = res['data'];
        this._service.setStorage('editorDetails', [this.globalConfig]);
      })
    }
  }

  setUserEditedState(){
    this.userSavedHistory.forEach(e => {
      this.searchvalueByEditorId(e.element_id, e.value)
    });
  }
  searchvalueByEditorId(editorId, value){
    this.slideList.forEach((e)=>{
        e.elements.forEach((ele)=>{
          if (ele.id === editorId){
            ele.value = value;
          }
        })
    })
  }
  getSlideStartTime(res){
    res.forEach(e=>{
      this.options.stepsArray.push({value: Number(e.start_time)})
    });
    this.slideConfig.slidesToShow = this.options.stepsArray.length;
    this.value =  this.options.stepsArray[0].value;
  }
  /** Setup initital  */
  setInit(){
    const { filename_image_url, elements } = this.slideList[0];
    if(this.slideList[0] !==undefined){
      this.initDevice.backgroundImage = filename_image_url;
      setTimeout(() => {
        let elem = document.querySelectorAll('.slick-initialized .slick-slide')[0];
        let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
        elem.dispatchEvent(evt);
        const result = elements.filter((item) => {
          return item;
        });
        this.activeTabOnLoad(result[0].type + '_tab');

      }, 100)
    }else{
      location.reload();
    }

  }
  onUserChangeEnd(e){
    const slideIndex = this._http.findIndexInData({ data: this.options.stepsArray, where: 'value', what: e.value });
    let elem = document.querySelectorAll('.slick-initialized .slick-slide')[slideIndex];
    let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
    elem.dispatchEvent(evt);
    const type = $('.deviceHtml div').eq(0).attr('type');
    this.activeTabOnLoad(type + '_tab');

  }
  //Device Basic Html

  deviceHtml(){

  }
  /*
    Select slide
  */
  selectSlide(index, id, img_url){
    this.initDevice.backgroundImage = img_url;
    const value = this.options.stepsArray[index].value;
    this.value = value;
    this.findElementBySlideId(id);
    const type = $('.deviceHtml div').eq(0).attr('type');
    this.activeTabOnLoad(type + '_tab');
  }
  findElementBySlideId(id){
     const ele=  this.slideList.filter((item)=>{
        if(item.id === id){
          return item.elements;
        }
      });
      $('.deviceHtml').html('');
      ele[0].elements.forEach(e => {
        this.filterByElementType(e.type, e)
      });
      this.gloablFunctions();
  }

  filterByElementType(type, response){
    switch (type) {
      case "video":
        this.createVideoElement(response, type);
      break;
      case "text":
        this.createTextElement(response, type);
      break;
      case "image":
        this.createImageElement(response, type);
      break;
      default:
      break;
    }
  }

  createTextElement(response, type){
    const { value, properties_y_position,
             properties_text_max,
             slide_id, id,
             properties_text_min,
             properties_text_font_color,
             properties_text_font_size,
             properties_horizontal_aligned}   = response;
    let txtHtml = `<div
                    style='top: ${properties_y_position / 5}px; border-color:  ${properties_text_font_color+'7a'}'
                    type='${type}'
                    value="${value}"
                    class='txtinput  ${properties_horizontal_aligned}'>
                    <input type='text'
                    style='color:  ${properties_text_font_color}; font-size: ${properties_text_font_size/4.70}px'
                    min=${properties_text_min}
                    maxlength=${properties_text_max}
                    value="${value}">
                    <div class="saveText" style="display:none">
                      <a href="javascript:;"
                      class="saveToDB"
                      slide_id="${slide_id}"
                      elementId="${id}"
                      type="${type}"
                      style="background-color: ${properties_text_font_color}">save</a>
                    </div>
                    <div>`

    $('.deviceHtml').append(txtHtml)
    // this.renderer.appendChild(this.mobileHTML.nativeElement, txtHtml)
  }
  createVideoElement(response, type){
    const { properties_y_position, slide_id, id } = response;
    let txtHtml = `<div class='videoBox' type='${type}' style='top: ${properties_y_position == 0 ? 'auto' : properties_y_position}px'>
                    <div class="row">
                    <div class="small-12 medium-2 large-2 columns">
                      <div class="circle blink">
                        <i class="fa fa-video-camera upload-button-video" ></i>
                          <input class="file-upload video" type="file"  slide_id="${slide_id}"  elementId="${id}"  accept="video/*"/>
                      </div>
                    </div>
                  </div>
                  </div>`;
    $('.deviceHtml').append(txtHtml)
  }
  createImageElement(response, type){
    const { properties_y_position, slide_id, id} = response;
    let txtHtml = `<div class='imgBox' type='${type}' style='top: ${properties_y_position == 0 ? 'auto' : properties_y_position}px'>
                    <div class="row">
                    <div class="small-12 medium-2 large-2 columns  ">
                      <div class="circle blink">
                        <i class="fa fa-camera upload-button-img"></i>
                          <input class="file-upload image" type="file" slide_id="${slide_id}"  elementId="${id}" accept="image/*"/>
                      </div>
                    </div>
                  </div>
                  </div>`;
      $('.deviceHtml').append(txtHtml)
  }


  /*** Save to Db */

  saveChanges(slideId, elementId, type, value){
    const self = this
    const {  user_id,  campaign_id,  theme_id,  id } = this.globalConfig;
    self._http.saveUserChanges( user_id,campaign_id, theme_id, slideId, elementId,  id, type, value).subscribe(res=>{
      self._msg.success('Element has been sucessfully updated');
    },(error)=>{
      self._msg.error('Something went wrong. Please try again');
    })


  }


  /** Active  */
  activeTabOnLoad(name){
    let elem = document.getElementById(name);
    let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
    elem.dispatchEvent(evt);
  }

  gloablFunctions(){
    const self = this;
    $('.txtinput input').on('keyup',function(e){
      let minLength = $(this).attr('min');
      let initValue = $(this).parent().attr('value');

      if (e.target.value.length < minLength){
        self._msg.error('Length is short, minimum ' + minLength +' required.');
        return false;
      }
      if (e.target.value !== initValue){
        $(this).next().fadeIn();
      }else{
        $(this).next().hide();
      }
    });

    $('.saveText a').on('click', function(){
      const slideId = $(this).attr('slide_id');
      const elementId = $(this).attr('elementId');
      const type = $(this).attr('type');
      const value = $(this).parent().prev().val();
      self.searchUpdateById(slideId, elementId, value)
      self.saveChanges(slideId, elementId, type, value);
      $(this).parent().hide();
    })

    $('.txtinput input').on('focus', ()=>{
      let elem = document.getElementById('text_tab');
      let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
      elem.dispatchEvent(evt);
    })

    $(".upload-button-img").on('click', function () {
        $(this).next().click();
      //self.open(this.modelImg)
      let elem = document.getElementById('image_tab');
      let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
      elem.dispatchEvent(evt);
    });
    $(".upload-button-video").on('click', function () {
      $(this).next().click();
      let elem = document.getElementById('video_tab');
      let evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
      elem.dispatchEvent(evt);
    });
    $('.file-upload.video').change(function (e) {
      const slideId = $(this).attr('slide_id');
      const elementId = $(this).attr('elementId');
      const fileName = e.target.files[0];
      self.uploadvideo(elementId, slideId, fileName)
    });
    $('.file-upload.image').change(function (e) {
      const slideId = $(this).attr('slide_id');
      const elementId = $(this).attr('elementId');
      const fileName = e.target.files[0];
      self.uploadimage(elementId, slideId, fileName)
    });
  }

  uploadvideo(elementId, slideId, name){
      const { user_id, campaign_id, theme_id, id } = this.globalConfig;
      var form = new FormData();
      form.append("userId", user_id);
      form.append("campaignId", campaign_id);
      form.append("themeId", theme_id);
      form.append("slideId", slideId);
      form.append("elementId", elementId);
      form.append("editorId", id);
      form.append("videoFile", name);
      this._req.post(`${this.config.url}/upload/video`, form).subscribe(res => {
        if(res){
          this.saveChanges(slideId, elementId, 'video', res['statusData'].data.fileName);
        }
      }, (e) => {
            this._msg.error(e.error.statusMessage);
      });
  }

  uploadimage(elementId, slideId, name) {

    const { user_id, campaign_id, theme_id, id } = this.globalConfig;
    var form = new FormData();
    form.append("userId", user_id);
    form.append("campaignId", campaign_id);
    form.append("themeId", theme_id);
    form.append("slideId", slideId);
    form.append("elementId", elementId);
    form.append("editorId", id);
    form.append("imageFile", name);
    this._req.post(`${this.config.url}/upload/image`, form).subscribe(res => {
      if (res) {
        this.saveChanges(slideId, elementId, 'image', res['statusData'].data.fileName);
      }
    }, (e) => {
      this._msg.error(e.error.statusMessage);
    });
  }

  // search and update value in stored array
  searchUpdateById(slideId, elementId, value){
    this.slideList.forEach(e => {
      if(e.id==slideId){
          e.elements.forEach(ele => {
              if(ele.id == elementId){
                ele.value = value;
              }
          });
      }
    });
  }
  saveSync(){
    this.showloader = true;
    const { id, campaign_id, user_id, theme_id } = JSON.parse(localStorage.getItem('userDetails')).editorDetails[0]
    this._req.post(`${this.config.url}/editor/save`, { 'userId': user_id, 'campaignId': campaign_id, 'themeId': theme_id, 'editorId': id }).subscribe(res => {
      if (res) {

        this._msg.success(res['statusMessage']);
        this.showloader = false;
      }
    }, (e) => {
      this._msg.error(e.error.statusMessage);
        this.showloader = false;
    });
  }

  isVideoExist(){
    if ($('.deviceHtml div').hasClass('videoBox')){
      $('.file-upload').click();
    }else{
      this._msg.error('Your current slide does not have video');
    }
  }
  isImageExist(){
    if ($('.deviceHtml div').hasClass('imgBox')) {
      $('.file-upload').click();
    } else {
      this._msg.error('Your current slide does not have image');
    }
  }



}
