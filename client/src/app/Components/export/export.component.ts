import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../../Shared/Services/template.service';
import { MessageService } from 'src/app/Shared/Services/message.service';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../Shared/Services/config';

declare var $:any

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {
  interval;
  config = new Config;
  isloading = false;
  fileName = '';
  isDownloaded = false;
  constructor(private _http: TemplateService,
    private _req: HttpClient,
    public _msg: MessageService) { }

  ngOnInit() {
  }
  ngOnDestroy(){
    window.clearInterval(this.interval);
  }
  exportVideo(){
    const { id, campaign_id, user_id, theme_id } = JSON.parse(localStorage.getItem('userDetails')).editorDetails[0]
    this._req.post(`${this.config.url}/editor/render`, { 'userId': user_id, 'campaignId': campaign_id, 'themeId': theme_id, 'editorId': id }).subscribe(res => {
      if (res) {
        this._msg.success(res['statusMessage']);
        this.isloading = true;
        // put checkStatus function here
        this.checkStatus();
      }
    }, (e) => {
      this._msg.error(e.error);
      this.isloading = false;
    });

  }

  checkStatus(){
    const { id, campaign_id, user_id, theme_id } = JSON.parse(localStorage.getItem('userDetails')).editorDetails[0]
    this.isloading = true;
    this.interval = window.setInterval(() => {
      this._req.post(`${this.config.url}/editor/status`, { 'userId': user_id, 'campaignId': campaign_id, 'themeId': theme_id, 'editorId': id }).subscribe(res => {
        if (res) {
          if(res)
            if (res['data'].rendered_status){
              this.isloading = false;
              this.isDownloaded = true;
              this.fileName = res['data'].filename_url;
              this._msg.success(res['statusMessage']);
              this.playVideo();
              window.clearInterval(this.interval);
            }
        }
      }, (e) => {
        this._msg.error(e.error);
      });
    }, 5000);
  }
  downloadFile(){
    window.open(this.fileName);

  }
  playVideo(){
    const videoHTML = `
                       <video width="100%" src="${this.fileName}" height="100%" class="videoPlayer" controls autoplay>
                        </video>
                      `
    $('.deviceHtml').removeAttr('style');
    $('.deviceHtml').html(videoHTML);
    window.clearInterval(this.interval);
  }

}
