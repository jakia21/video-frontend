import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTemplateWidgetComponent } from './edit-template-widget.component';

describe('EditTemplateWidgetComponent', () => {
  let component: EditTemplateWidgetComponent;
  let fixture: ComponentFixture<EditTemplateWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTemplateWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTemplateWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
