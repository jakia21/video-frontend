import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { CampaignService } from '../../../Shared/Services/campaign.service';
declare var $: any;

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent implements OnInit {
  data : any;
  selected: any;
  constructor(private _http: CampaignService, private router : Router) { }

  ngOnInit() {
    this.getPlatform();
    window['_platform'] = this;
  }

  ngAfterViewInit(){
    const current = JSON.parse(localStorage.getItem('userDetails')).plateforms;
    setTimeout(()=>{
      $('.tab-platform li').eq(current - 1).find('a').addClass('active');
      $('.tab-platform li a').click(function(){
        $('.tab-platform li a').removeClass('active');
        $(this).addClass('active');
      })
    }, 100)

  }
  setPlatform(e, name){
    this._http.setStorage('plateforms', String(name));
  }

  getPlatform(){
    const platforms = JSON.parse(localStorage.getItem('userDetails')).listPlateforms;
    const current = JSON.parse(localStorage.getItem('userDetails')).userJourney.selectedTheme[0].plateform_id

    // initally we are showing only one theme and also showing a single theme.

    const activePlatform = platforms.filter((item)=>{
        return item.id == Number(current)
    })
    this.data = activePlatform;
  }
  refreshPage(){
    this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() =>
    this.router.navigate(["/dashboard/change-template"]));
  }




}
