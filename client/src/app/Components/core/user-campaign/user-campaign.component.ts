import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-campaign',
  templateUrl: './user-campaign.component.html',
  styleUrls: ['./user-campaign.component.css']
})
export class UserCampaignComponent implements OnInit {
  data : any;
  constructor() { }

  ngOnInit() {
    const usrCampaign = JSON.parse(localStorage.getItem('userDetails')).currentUserCampaigns;
    this.data = usrCampaign;

  }

}
