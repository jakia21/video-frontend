import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-widget',
  templateUrl: './template-widget.component.html',
  styleUrls: ['./template-widget.component.css']
})
export class TemplateWidgetComponent implements OnInit {
  @Input() activeTab: string;

  details = {
    brand: '',
    name: '',
    product: '',
    platform_img: '',
    properties_duration: '',
    properties_aspect_ratio: '',
    subformat_id: 1,
    properties_height: '',
    properties_width: ''
  };

  step = {
    'cplatform' : false,
    'cformat' : false,
    'ctemplate': false,
    'etemplate' : false,
    'emusic' : false,
    'export' : false
  }


  constructor() { }

  ngOnInit() {
    this.bindCampaignDetails();
  }
  bindCampaignDetails() {
    const { campignDetails, userJourney } = JSON.parse(localStorage.getItem('userDetails'));
    Object.assign(this.details, { 'brand': campignDetails[0].brand, 'name': campignDetails[0].name, 'product': campignDetails[0].product })
    Object.assign(this.details, {
      properties_duration: userJourney.selectedTheme[0].properties_duration,
      properties_aspect_ratio: userJourney.selectedTheme[0].properties_aspect_ratio,
      subformat_id: userJourney.selectedTheme[0].subformat_id,
      platform_img: userJourney.selectedTemplate.platform_img,
      properties_height: userJourney.selectedTheme[0].properties_height,
      properties_width: userJourney.selectedTheme[0].properties_width
    });
    this.setCurrentActive();
  }
  receiveEvent(e) {
    this.bindCampaignDetails();
  }
  setCurrentActive(){
      this.step[this.activeTab] = true;
  }


}
