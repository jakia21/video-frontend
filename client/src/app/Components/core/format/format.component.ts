import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CampaignService } from '../../../Shared/Services/campaign.service';
@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.css']
})
export class FormatComponent implements OnInit {
  @Output() updateFormmat = new EventEmitter<number>();

  data:any;
  constructor(private _http: CampaignService) { }

  ngOnInit() {
    this.fetchFormat();
  }

  fetchFormat(){
    this._http.getFormat().subscribe(response => {
      this.data = response.statusData.data;
    })
  }
  setFormat(name){
    this._http.setStorage('formats', String(name));
    this.updateFormmat.emit(name)
  }


}
