import { Component, OnInit, Input } from '@angular/core';
import { CampaignService } from '../../../Shared/Services/campaign.service';
declare var $:any;
@Component({
  selector: 'app-preview-device',
  templateUrl: './preview-device.component.html',
  styleUrls: ['./preview-device.component.css']
})
export class PreviewDeviceComponent implements OnInit {
  @Input() url : string
  constructor(private _http: CampaignService) { }

  ngOnInit() {
    const html = JSON.parse(localStorage.getItem('device'));
    $('.mockup-content').html(html);
  }

  routeToNext(){
    this._http.routeToNext(this.url);
  }

}
