import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformSidebarComponent } from './platform-sidebar.component';

describe('PlatformSidebarComponent', () => {
  let component: PlatformSidebarComponent;
  let fixture: ComponentFixture<PlatformSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
