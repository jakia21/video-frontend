import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CampaignService } from '../../../Shared/Services/campaign.service';
@Component({
  selector: 'app-platform-sidebar',
  templateUrl: './platform-sidebar.component.html',
  styleUrls: ['./platform-sidebar.component.css']
})
export class PlatformSidebarComponent implements OnInit {
  @Output() changeEvent = new EventEmitter<string>();

  data: any;
  constructor(private _http: CampaignService) { }

  ngOnInit() {
    this.fetchPlatform();
  }
  async fetchPlatform() {
   this._http.getPlatform().subscribe(response => {
      const res =  response.statusData.data;
      this.setActivePlatform(res);
    });
  }
  setActivePlatform(res){
    const current = JSON.parse(localStorage.getItem('userDetails')).userJourney.selectedTheme[0].plateform_id;
    const activePlatform = res.filter((item) => {
      return item.id == Number(current)
    })
    this.data = activePlatform;
  }
  setPlatform(name){
   // this._http.setStorage('plateforms', String(name));
   // this.setPlatformImage(name);
    this.changeEvent.emit(String(name))
  }
  // setPlatformImage(id){
  //   const result = this.data.filter((item)=>{ return item.id === id;});

  //   this._http.setStorage('platform_img', result[0].image_url);
  // }

}
