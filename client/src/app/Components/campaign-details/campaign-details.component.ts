import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { CampaignService } from '../../Shared/Services/campaign.service';
import { MessageService } from '../../Shared/Services/message.service';
import { Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.css']
})
export class CampaignDetailsComponent implements OnInit {
  isloaded = false;
  submittedOnce = false;
  campaignForm: FormGroup;
  categoryList : any;
  constructor(private _http: CampaignService,
              private formBuilder: FormBuilder,
              private _msg: MessageService,
              private routes: Router
              ) { }

  ngOnInit() {
    this.campaignForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        brand: ['', [Validators.required]],
        product: ['', Validators.required],
        categoryId: [Number('') + 1, Validators.required],
        description: ['', Validators.required],
        userId: [1]
    });
    this._http.getCategoryList().subscribe(response =>{
      this.categoryList = response.statusData.data;
      this.isloaded = true;
    })
  }
  get f() {
    return this.campaignForm.controls;
  }
  ngAfterViewInit(){
  }
  onSubmit(){
    this.submittedOnce = true;
    if (this.campaignForm.status === 'VALID'){
      this._http.createCampaign(this.campaignForm.value).subscribe(response =>{
        if (response){
          this.setStorage(response.statusData.data)
          this._msg.success(response.statusMessage);
          setTimeout(() => {
            window['_sidebar'].checkIfCampignExists();
            this.routes.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() =>
            this.routes.navigate(['/dashboard/change-platform']));
          },1000);
        }
      },(error)=>{;
        this._msg.error('error', 'Error',error);
      })
    }

  }

  setStorage(value){
    this._http.setStorage('campignDetails',[value])
  }


}
