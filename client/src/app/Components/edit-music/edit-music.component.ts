import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TemplateService } from 'src/app/Shared/Services/template.service';
import { Config } from '../../Shared/Services/config';
import { MessageService } from 'src/app/Shared/Services/message.service';

declare var $:any;

@Component({
  selector: 'app-edit-music',
  templateUrl: './edit-music.component.html',
  styleUrls: ['./edit-music.component.css']
})
export class EditMusicComponent implements OnInit {
  config = new Config;
  url = '';
  fileName = '';
  fileState = {
    original : true,
    uploaded: false,
    fileName : ''
  }
  isloaded = false;
  constructor(
    private _http: TemplateService,
    private _req: HttpClient,
    private _msg : MessageService
  ) { }

  ngOnInit() {
    this.fetchAudio();
  }

  fetchAudio(){
    const { category_id, id} = JSON.parse(localStorage.getItem('userDetails')).userJourney.selectedTemplate;
    this._http.getThemeListById(category_id, id).subscribe(res=>{
      this.url = res.statusData.data[0].audio_file_url;
      this.fileName = this.url.split('/')[this.url.split('/').length - 1];
      this.globalFunction();
      this.isloaded = true;
    })
  }
  globalFunction(){
    const self = this;
    $('.music-content-right p a.replace').on('click', function(){
      $(this).next().click();
    });
    $('.file-audio').on('change',function (e) {
      const fileName = e.target.files[0];
      self.uploadAudio(fileName)
    });
  }
  uploadAudio(name) {
    const { user_id, campaign_id, theme_id, id } = JSON.parse(localStorage.getItem('userDetails')).editorDetails[0];
    var form = new FormData();
    form.append("userId", user_id);
    form.append("campaignId", campaign_id);
    form.append("themeId", theme_id);
    form.append("editorId", id);
    form.append("audioFile", name);
    this._req.post(`${this.config.url}/upload/audio`, form).subscribe(res => {
      if (res) {
        this._msg.success(res['statusMessage']);
        this.fileState.fileName = res['data'].audio_file_name;
        this.fileState.original = false;
        this.fileState.uploaded = true;
      }
    },error=>{
       this._msg.error(error);
    });

  }
  deleteUploaded(){
    this.fileState.original = true;
    this.fileState.uploaded = false;

  }
}
